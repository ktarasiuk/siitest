﻿namespace OfficeBook.Common.Interfaces.Business
{
    using Model;

    public interface IBookingService
    {
        Booking FindById(long id);
    }
}
