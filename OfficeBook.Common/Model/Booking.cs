﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OfficeBook.Common.Model
{
    public class Booking
    {
        public long Id { get; set; }
        public DateTime CreateDateTime { get; set; }
    }
}
