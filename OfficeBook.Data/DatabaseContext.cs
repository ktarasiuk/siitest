﻿using Microsoft.EntityFrameworkCore;
using OfficeBook.Data.Model;
using System;

namespace OfficeBook.Data
{
    public class DatabaseContext: DbContext
    {
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            ConfigureModelMappging(modelBuilder);
            base.OnModelCreating(modelBuilder);
        }

        private void ConfigureModelMappging(ModelBuilder modelBuilder)
        {
            //var booking = modelBuilder.Entity<Booking>();

            //booking.Property(c => c.Id);
            //booking.Property(c => c.CreateDateTime).HasDefaultValue(DateTime.UtcNow);

            //booking.HasKey(c => c.Id);
            //booking.ToTable("Bookings");
        }
    }
}
