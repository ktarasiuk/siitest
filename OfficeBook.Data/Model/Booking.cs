﻿using System;

namespace OfficeBook.Data.Model
{
    public class Booking
    {
        public long Id { get; set; }
        public DateTime CreateDateTime { get; set; }
    }
}
