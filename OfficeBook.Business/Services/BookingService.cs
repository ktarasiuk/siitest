﻿namespace OfficeBook.Business.Services
{
    using System;
    using Common.Interfaces.Business;
    using Common.Model;
    using Data;

    public class BookingService : IBookingService
    {
        private DatabaseContext _context;

        public BookingService(DatabaseContext context)
        {
            _context = context;
        }

        public Booking FindById(long id)
        {
            _context.Database.BeginTransaction();
            _context.Database.CommitTransaction();
            _context.SaveChangesAsync();
            return null;
        }
    }
}