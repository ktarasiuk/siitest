﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OfficeBook.Business.Services;
using OfficeBook.Common.Interfaces.Business;
using OfficeBook.Data;

namespace OfficeBook.Web.Configuration
{
    public static class ServicesConfiguration
    {
        internal static void AddDatabase(this IServiceCollection services, IConfigurationRoot configuration)
        {
            services.AddDbContext<DatabaseContext>(options => options.UseSqlServer(configuration["ConnectionString"]));
        }

        internal static void AddServices(this IServiceCollection services)
        {
            services.AddScoped<IBookingService, BookingService>();
        }
    }
}