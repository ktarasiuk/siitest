﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OfficeBook.Common.Interfaces.Business;

namespace OfficeBook.Web.Controllers
{
    [Route("")]
    public class HomeController : Controller
    {
        private IBookingService _bookingService;
        public HomeController(IBookingService bookingService)
        {
            _bookingService = bookingService;
        }
        [HttpGet("")]
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
